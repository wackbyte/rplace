//! Tools for reading and writing timestamps.

use {
    std::io::Write,
    time::{format_description::FormatItem, macros::format_description, PrimitiveDateTime},
};

/// The timestamp format with subseconds.
pub const TIMESTAMP_FORMAT_WITH_SUBSECOND: &[FormatItem<'static>] =
    format_description!("[year]-[month]-[day] [hour]:[minute]:[second].[subsecond] UTC");

/// The timestamp format without subseconds.
pub const TIMESTAMP_FORMAT_WITHOUT_SUBSECOND: &[FormatItem<'static>] =
    format_description!("[year]-[month]-[day] [hour]:[minute]:[second] UTC");

/// Get the format to use for a timestamp.
///
/// If the timestamp has milliseconds, [`TIMESTAMP_FORMAT_WITH_SUBSECOND`] is used.
/// Otherwise, [`TIMESTAMP_FORMAT_WITHOUT_SUBSECOND`] is.
pub fn format_for_timestamp(timestamp: PrimitiveDateTime) -> &'static [FormatItem<'static>] {
    if timestamp.millisecond() == 0 {
        TIMESTAMP_FORMAT_WITHOUT_SUBSECOND
    } else {
        TIMESTAMP_FORMAT_WITH_SUBSECOND
    }
}

/// Try to read a timestamp.
pub fn read_timestamp(source: &str) -> Result<PrimitiveDateTime, time::error::Parse> {
    PrimitiveDateTime::parse(source, TIMESTAMP_FORMAT_WITH_SUBSECOND)
        .or_else(|_| PrimitiveDateTime::parse(source, TIMESTAMP_FORMAT_WITHOUT_SUBSECOND))
}

/// Try to write a timestamp.
pub fn write_timestamp<W>(
    timestamp: PrimitiveDateTime,
    output: &mut W,
) -> Result<usize, time::error::Format>
where
    W: Write,
{
    timestamp.format_into(output, format_for_timestamp(timestamp))
}

/// Write a timestamp to a new string.
pub fn write_timestamp_to_string(
    timestamp: PrimitiveDateTime,
) -> Result<String, time::error::Format> {
    timestamp.format(format_for_timestamp(timestamp))
}
