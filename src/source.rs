//! Tools for reading and writing events.

use {
    self::timestamp::{read_timestamp, write_timestamp, write_timestamp_to_string},
    crate::{Area, Color, Event, Point, Rect},
    std::{
        fmt::{self, Display, Formatter},
        io::{self, Write},
    },
};

pub mod timestamp;

/// An error occured while parsing.
///
/// # Why would you do this
///
/// ...Let's just say this library isn't focused on error handling.
/// It's only meant to parse the official datasets.
/// If you are running into mysterious parsing errors, feel free to open an issue.
#[derive(Copy, Clone, Debug, Default, Eq, PartialEq, Ord, PartialOrd, Hash)]
pub struct Error;

impl Display for Error {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        write!(f, "r/place data parsing error")
    }
}

impl std::error::Error for Error {}

/// A [`Result`](std::result::Result) that comes with [`Error`].
pub type Result<T> = std::result::Result<T, Error>;

/// Read a single event from one row of CSV data.
pub fn read_one<'a>(source: &'a str) -> Result<Event<'a>> {
    // Split it into its four parts.
    let mut parts = source.splitn(4, ',');

    // The subseconds in Reddit's date format seem to be optional, so we have to try both.
    // Maybe there's a way to do this with only one description?
    let timestamp = parts.next().ok_or(Error)?;
    let extracted_timestamp = read_timestamp(timestamp).map_err(|_| Error)?;

    // The user ID is just a string.
    let user = parts.next().ok_or(Error)?;

    // Extract the color.
    let color = parts.next().ok_or(Error)?;
    // All colors are 7 bytes long. (`#000000`)
    if color.len() != 7 {
        return Err(Error);
    }
    // Extract and parse its components.
    let r = u8::from_str_radix(&color[1..3], 16).map_err(|_| Error)?;
    let g = u8::from_str_radix(&color[3..5], 16).map_err(|_| Error)?;
    let b = u8::from_str_radix(&color[5..7], 16).map_err(|_| Error)?;
    // Create the color.
    let extracted_color = Color::new(r, g, b);

    // Extract the area.
    let area = parts.next().ok_or(Error)?;
    // No area can be shorter than `"0,0"`, which is 5 bytes long.
    if area.len() < 5 {
        return Err(Error);
    }
    // Extract and split the portion between the quotes.
    let content = area.get(1..(area.len() - 1)).unwrap();
    let mut coordinates = content.splitn(4, ',');
    // Then parse the first two coordinates.
    let x1 = coordinates
        .next()
        .ok_or(Error)?
        .parse()
        .map_err(|_| Error)?;
    let y1 = coordinates
        .next()
        .ok_or(Error)?
        .parse()
        .map_err(|_| Error)?;
    // Create the area. If the extra 2 coordinates are available, it is a rectangle.
    let extracted_area = if let Some(x2) = coordinates.next() {
        let x2 = x2.parse().map_err(|_| Error)?;
        let y2 = coordinates
            .next()
            .ok_or(Error)?
            .parse()
            .map_err(|_| Error)?;
        Area::Rect(Rect::new(x1, y1, x2, y2))
    } else {
        Area::Point(Point::new(x1, y1))
    };

    Ok(Event {
        timestamp: extracted_timestamp,
        user,
        color: extracted_color,
        area: extracted_area,
    })
}

/// Write a single event.
pub fn write_one<'a, W>(
    Event {
        timestamp,
        user,
        color,
        area,
    }: &Event<'a>,
    output: &mut W,
) -> io::Result<()>
where
    W: Write,
{
    // TODO: We probably don't need to handle the `InsufficientTypeInformation` error?
    if let Err(time::error::Format::StdIo(e)) = write_timestamp(*timestamp, output) {
        return Err(e);
    }
    match area {
        Area::Point(Point { x, y }) => write!(output, ",{user},{color},\"{x},{y}\""),
        Area::Rect(Rect { x1, y1, x2, y2 }) => {
            write!(output, ",{user},{color},\"{x1},{y1},{x2},{y2}\"")
        }
    }
}

/// Write a single event to a string.
pub fn write_one_to_string<'a>(
    Event {
        timestamp,
        user,
        color,
        area,
    }: &Event<'a>,
    output: &mut String,
) {
    // TODO: Is it okay to unwrap this, or should the error be handled?
    output.push_str(&write_timestamp_to_string(*timestamp).expect("failed to format timestamp"));
    output.push_str(&match area {
        Area::Point(Point { x, y }) => {
            format!(",{user},{color},\"{x},{y}\"")
        }
        Area::Rect(Rect { x1, y1, x2, y2 }) => {
            format!(",{user},{color},\"{x1},{y1},{x2},{y2}\"")
        }
    });
}

/// Read a list of events from CSV data.
pub fn read<'a>(source: &'a str) -> Result<Vec<Event<'a>>> {
    // Skip the first line and parse the rest.
    source.lines().skip(1).map(|line| read_one(line)).collect()
}

/// Write a list of events as CSV.
///
/// Doesn't come with a trailing newline.
pub fn write<'a, W>(events: &[Event<'a>], output: &mut W) -> io::Result<()>
where
    W: Write,
{
    write!(output, "timestamp,user_id,pixel_color,coordinate")?;
    for event in events {
        write!(output, "\n")?;
        write_one(event, output)?;
    }
    Ok(())
}

/// Write a list of events as CSV to a string.
///
/// Doesn't come with a trailing newline.
pub fn write_to_string<'a>(events: &[Event<'a>], output: &mut String) {
    output.push_str("timestamp,user_id,pixel_color,coordinate");
    for event in events {
        output.push('\n');
        write_one_to_string(event, output);
    }
}

#[cfg(test)]
mod tests {
    use {super::*, time::macros::datetime};

    fn test_read_one(source: &str, expected: Event) {
        assert_eq!(read_one(source), Ok(expected));
    }

    fn test_write_one(event: Event, expected: &str) {
        // `write_one`
        {
            let mut buffer = Vec::new();
            write_one(&event, &mut buffer).unwrap();
            let source = String::from_utf8_lossy(&buffer);
            assert_eq!(source, expected);
        }

        // `write_one_to_string`
        {
            let mut buffer = String::new();
            write_one_to_string(&event, &mut buffer);
            assert_eq!(buffer, expected);
        }
    }

    fn test_read(source: &str, expected: Vec<Event>) {
        assert_eq!(read(source), Ok(expected));
    }

    fn test_write(events: Vec<Event>, expected: &str) {
        // `write`
        {
            let mut buffer = Vec::new();
            write(&events, &mut buffer).unwrap();
            let source = String::from_utf8_lossy(&buffer);
            assert_eq!(source, expected);
        }

        // `write_to_string`
        {
            let mut buffer = String::new();
            write_to_string(&events, &mut buffer);
            assert_eq!(buffer, expected);
        }
    }

    // Data taken from line 2 of the first dataset.
    #[test]
    fn read_one_point() {
        let source = r#"2022-04-03 17:38:20.021 UTC,p0sXpmkcmg1KLiCdK5e4xKdudb1f8cjscGs35082sKpGBfQIw92nZ7yGvWbQ/ggB1+kkRBaYu1zy6n16yL/yjA==,#FF4500,"371,488""#;
        let expected = Event {
            timestamp: datetime!(2022-04-03 17:38:20.021),
            user: "p0sXpmkcmg1KLiCdK5e4xKdudb1f8cjscGs35082sKpGBfQIw92nZ7yGvWbQ/ggB1+kkRBaYu1zy6n16yL/yjA==",
            color: Color::new(255, 69, 0),
            area: Area::Point(Point::new(371, 488)),
        };
        test_read_one(source, expected)
    }

    // Data taken from line 785633 of the first dataset.
    #[test]
    fn read_one_point_no_subsecond() {
        let source = r#"2022-04-03 17:57:21 UTC,XAZoby4fGRpzw4mT4vbCfgQb/FizMG0FTey9Alh4tAInA38QVWgQgDFFCpqs9JthZ6QxSPJ3l+5MxI+wQShLYQ==,#00CC78,"1516,974""#;
        let expected = Event {
            timestamp: datetime!(2022-04-03 17:57:21),
            user: "XAZoby4fGRpzw4mT4vbCfgQb/FizMG0FTey9Alh4tAInA38QVWgQgDFFCpqs9JthZ6QxSPJ3l+5MxI+wQShLYQ==",
            color: Color::new(0, 204, 120),
            area: Area::Point(Point::new(1516, 974)),
        };
        test_read_one(source, expected);
    }

    // Data taken from line 3 of the first dataset.
    #[test]
    fn write_one_point() {
        let event = Event {
            timestamp: datetime!(2022-04-03 17:38:20.024),
            user: "Ctar52ln5JEpXT+tVVc8BtQwm1tPjRwPZmPvuamzsZDlFDkeo3+ItUW89J1rXDDeho6A4zCob1MKmJrzYAjipg==",
            color: Color::new(81, 233, 244),
            area: Area::Point(Point::new(457, 493))
        };
        let expected = r#"2022-04-03 17:38:20.024 UTC,Ctar52ln5JEpXT+tVVc8BtQwm1tPjRwPZmPvuamzsZDlFDkeo3+ItUW89J1rXDDeho6A4zCob1MKmJrzYAjipg==,#51E9F4,"457,493""#;
        test_write_one(event, expected);
    }

    // Data taken from line 909389 of the first dataset.
    #[test]
    fn write_one_point_no_subsecond() {
        let event = Event {
            timestamp: datetime!(2022-04-03 17:59:58),
            user: "0593gBauSdE1MHv3FyvrDUYZ2xfQA6pY6fE4/v2OuBm27HpAxRV2ZxV4uYeV0CKolYz4eGs3rB2LepHuK5KIqg==",
            color: Color::new(0, 0, 0),
            area: Area::Point(Point::new(1995, 582)),
        };
        let expected = r#"2022-04-03 17:59:58 UTC,0593gBauSdE1MHv3FyvrDUYZ2xfQA6pY6fE4/v2OuBm27HpAxRV2ZxV4uYeV0CKolYz4eGs3rB2LepHuK5KIqg==,#000000,"1995,582""#;
        test_write_one(event, expected);
    }

    // Data taken from lines 4-6 of the first dataset.
    #[test]
    fn read_points() {
        let source = r#"timestamp,user_id,pixel_color,coordinate
2022-04-03 17:38:20.025 UTC,rNMF5wpFYT2RAItySLf9IcFZwOhczQhkRhmTD4gv0K78DpieXrVUw8T/MBAZjj2BIS8h5exPISQ4vlyzLzad5w==,#000000,"65,986"
2022-04-03 17:38:20.025 UTC,u0a7l8hHVvncqYmav27EARAE6ciLtpUTPXMI33lDrUmtj5Ei3ixlfRuG28KUvs7r5LpeiE/iOKPALVjkILhrYg==,#3690EA,"73,961"
2022-04-03 17:38:20.026 UTC,L8P+AXoFbbXPh2zBAkkXk96UrkKpB5hLq5gBMwvgSV0H4foa10rSyCwzWFrfvF2MUspFSJ9FkzNzY1//dL5u9A==,#FF4500,"1865,290""#;
        let expected = vec![
            Event {
                timestamp: datetime!(2022-04-03 17:38:20.025),
                user: "rNMF5wpFYT2RAItySLf9IcFZwOhczQhkRhmTD4gv0K78DpieXrVUw8T/MBAZjj2BIS8h5exPISQ4vlyzLzad5w==",
                color: Color::new(0, 0, 0),
                area: Area::Point(Point::new(65, 986)),
            },
            Event {
                timestamp: datetime!(2022-04-03 17:38:20.025),
                user: "u0a7l8hHVvncqYmav27EARAE6ciLtpUTPXMI33lDrUmtj5Ei3ixlfRuG28KUvs7r5LpeiE/iOKPALVjkILhrYg==",
                color: Color::new(54, 144, 234),
                area: Area::Point(Point::new(73, 961)),
            },
            Event {
                timestamp: datetime!(2022-04-03 17:38:20.026),
                user: "L8P+AXoFbbXPh2zBAkkXk96UrkKpB5hLq5gBMwvgSV0H4foa10rSyCwzWFrfvF2MUspFSJ9FkzNzY1//dL5u9A==",
                color: Color::new(255, 69, 0),
                area: Area::Point(Point::new(1865, 290)),
            },
        ];
        test_read(source, expected);
    }

    // Data taken from lines 7-9 of the first dataset.
    #[test]
    fn write_points() {
        let events = vec![
            Event {
                timestamp: datetime!(2022-04-03 17:38:20.027),
                user: "PNgTPhAA1zue68tZDNiqnNnKAsEyf4HSF826SBy5RHBebE+ocEHCh0wsOnAZtjdAWqfZyOR/Xb55zSiBSktBIw==",
                color: Color::new(255, 69, 0),
                area: Area::Point(Point::new(210, 513)),
            },
            Event {
                timestamp: datetime!(2022-04-03 17:38:20.028),
                user: "H9YaxLRMacKoLOjRnZ//0gJxUZ7JJAHPPahCReGZfxS88tZGT4e8tRkti55woOshlbdXc09zTvOao4uAcB/01A==",
                color: Color::new(0, 0, 0),
                area: Area::Point(Point::new(1334, 451)),
            },
            Event {
                timestamp: datetime!(2022-04-03 17:38:20.04),
                user: "2bQ05ZCKJMae5KYmnWzKnjc599ZtB8dpAjTbjpcy+ziVEYDZYjvyaMt0nyRZtdRTSdeHimUUwbPpqQuhmqh4wg==",
                color: Color::new(0, 0, 0),
                area: Area::Point(Point::new(78, 974)),
            }
        ];
        let expected = r#"timestamp,user_id,pixel_color,coordinate
2022-04-03 17:38:20.027 UTC,PNgTPhAA1zue68tZDNiqnNnKAsEyf4HSF826SBy5RHBebE+ocEHCh0wsOnAZtjdAWqfZyOR/Xb55zSiBSktBIw==,#FF4500,"210,513"
2022-04-03 17:38:20.028 UTC,H9YaxLRMacKoLOjRnZ//0gJxUZ7JJAHPPahCReGZfxS88tZGT4e8tRkti55woOshlbdXc09zTvOao4uAcB/01A==,#000000,"1334,451"
2022-04-03 17:38:20.04 UTC,2bQ05ZCKJMae5KYmnWzKnjc599ZtB8dpAjTbjpcy+ziVEYDZYjvyaMt0nyRZtdRTSdeHimUUwbPpqQuhmqh4wg==,#000000,"78,974""#;
        test_write(events, expected);
    }
}
