//! A crate for reading and writing [the data from Reddit's latest r/place experiment](https://www.reddit.com/r/place/comments/txvk2d/rplace_datasets_april_fools_2022/).

use {
    std::{
        cmp::Ordering,
        fmt::{self, Display, Formatter},
    },
    time::PrimitiveDateTime,
};

#[cfg(feature = "serde")]
use serde::{Deserialize, Serialize};

pub mod source;

/// A single point on the canvas.
#[derive(Copy, Clone, Debug, Default, Eq, PartialEq, Ord, PartialOrd, Hash)]
#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
pub struct Point {
    /// The X-coordinate in the range of `0..=1999`.
    pub x: u16,
    /// The Y-coordinate in the range of `0..=1999`.
    pub y: u16,
}

impl Point {
    /// Create a point from its coordinates.
    pub const fn new(x: u16, y: u16) -> Self {
        Self { x, y }
    }
}

impl Display for Point {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        write!(f, "({}, {})", self.x, self.y)
    }
}

impl From<(u16, u16)> for Point {
    fn from((x, y): (u16, u16)) -> Self {
        Self::new(x, y)
    }
}

impl From<Point> for (u16, u16) {
    fn from(point: Point) -> Self {
        (point.x, point.y)
    }
}

/// A range of points from an upper-left point to a lower-right point (inclusive).
#[derive(Copy, Clone, Debug, Default, Eq, PartialEq, Ord, PartialOrd, Hash)]
#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
pub struct Rect {
    /// The upper-left X-coordinate.
    pub x1: u16,
    /// The upper-left Y-coordinate.
    pub y1: u16,
    /// The lower-right X-coordinate.
    pub x2: u16,
    /// The lower-right Y-coordinate.
    pub y2: u16,
}

impl Rect {
    /// Create a rectangle from its coordinates.
    pub const fn new(x1: u16, y1: u16, x2: u16, y2: u16) -> Self {
        Self { x1, y1, x2, y2 }
    }

    /// Create a rectangle at a single point.
    pub const fn point(point: Point) -> Self {
        Self::new(point.x, point.y, point.x, point.y)
    }

    /// Get the upper-left point.
    pub const fn upper(self) -> Point {
        Point::new(self.x1, self.y1)
    }

    /// Get the lower-right point.
    pub const fn lower(self) -> Point {
        Point::new(self.x2, self.y2)
    }
}

impl Display for Rect {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        write!(f, "({}, {}, {}, {})", self.x1, self.y1, self.x2, self.y2)
    }
}

impl From<(u16, u16, u16, u16)> for Rect {
    fn from((x1, y1, x2, y2): (u16, u16, u16, u16)) -> Self {
        Self::new(x1, y1, x2, y2)
    }
}

impl From<Rect> for (u16, u16, u16, u16) {
    fn from(rect: Rect) -> Self {
        (rect.x1, rect.y1, rect.x2, rect.y2)
    }
}

impl From<Point> for Rect {
    fn from(point: Point) -> Self {
        Self::point(point)
    }
}

/// The area of the canvas an event occured at.
#[derive(Copy, Clone, Debug, Eq, PartialEq, Hash)]
#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
pub enum Area {
    /// A point. Used for normal pixel-placing events.
    ///
    /// See [`Point`].
    Point(Point),
    /// A rectangle. Used for the two censoring events performed by Reddit's moderators.
    ///
    /// See [`Rect`].
    Rect(Rect),
}

impl Area {
    /// Get the upper-left point.
    pub const fn upper(self) -> Point {
        match self {
            Self::Point(point) => point,
            Self::Rect(rect) => rect.upper(),
        }
    }

    /// Get the lower-right point.
    pub const fn lower(self) -> Point {
        match self {
            Self::Point(point) => point,
            Self::Rect(rect) => rect.lower(),
        }
    }
}

impl Default for Area {
    fn default() -> Self {
        Self::Point(Point::default())
    }
}

impl From<Point> for Area {
    fn from(point: Point) -> Self {
        Self::Point(point)
    }
}

impl From<Rect> for Area {
    fn from(rect: Rect) -> Self {
        Self::Rect(rect)
    }
}

/// The color of a pixel.
#[derive(Copy, Clone, Debug, Default, Eq, PartialEq, Ord, PartialOrd, Hash)]
#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
pub struct Color {
    /// The red component.
    pub r: u8,
    /// The green component.
    pub g: u8,
    /// The blue component.
    pub b: u8,
}

impl Color {
    /// Create a color from its components.
    pub const fn new(r: u8, g: u8, b: u8) -> Self {
        Self { r, g, b }
    }
}

impl Display for Color {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        write!(f, "#{:02X}{:02X}{:02X}", self.r, self.g, self.b)
    }
}

/// An event.
///
/// Ordered by timestamp.
#[derive(Copy, Clone, Debug, Eq, PartialEq, Hash)]
#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
pub struct Event<'a> {
    /// The time at which the event occured.
    ///
    /// Always UTC.
    pub timestamp: PrimitiveDateTime,
    /// The hashed identifier of the user who placed the pixel.
    pub user: &'a str,
    /// The color of the placed area.
    ///
    /// See [`Color`].
    pub color: Color,
    /// The area of the event.
    ///
    /// See [`Area`].
    pub area: Area,
}

impl<'a> PartialOrd for Event<'a> {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl<'a> Ord for Event<'a> {
    fn cmp(&self, other: &Self) -> Ordering {
        self.timestamp.cmp(&other.timestamp)
    }
}
