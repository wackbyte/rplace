# rplace

A crate for reading and writing [the data from Reddit's latest r/place experiment](https://www.reddit.com/r/place/comments/txvk2d/rplace_datasets_april_fools_2022/).

## Usage

Check the [`examples` directory](examples) for some usage examples.

The [unit tests](src/source.rs) may also be helpful.

## License

The code is in the public domain under the [Unlicense](UNLICENSE).
