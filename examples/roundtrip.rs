//! Reads from a file, decodes the data, and then reencodes it back into `stdout`.

use {
    rplace::source::{read, write},
    std::{
        env,
        fs::File,
        io::{stdout, Read, Write},
    },
};

fn main() {
    let path = env::args()
        .skip(1)
        .next()
        .expect("please provide a path argument");

    let source = {
        let mut file = File::open(path).expect("couldn't open that file");
        let mut buffer = String::new();
        file.read_to_string(&mut buffer)
            .expect("couldn't read that file");
        buffer
    };

    let events = read(&source).expect("couldn't parse the data");
    let mut stdout = stdout().lock();
    write(&events, &mut stdout).expect("couldn't write the data to stdout");
    let _ = stdout.flush();
}
